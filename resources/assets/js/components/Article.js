import Flickity from 'flickity';
import imagesLoaded from 'imagesloaded';
import Velocity from 'velocity-animate';
import { Helpers, addCSSRule, addClass, removeClass, getWindowHeight } from './Helpers';

class Article {

    static init() {
        Article.parallax();
        Article.renderAllGalleries();
        Article.fixArticleImages();
        Helpers.setSelectColor();
    }

    static parallax() {

        // Fade In Cover Image
        let cover = document.getElementsByClassName("portray__cover")[0];
        let articleHeader = document.getElementsByClassName("article__header")[0];
        let coverImage = document.getElementsByClassName("portray__cover__image")[0];
        let coverImageUrl = coverImage.getAttribute('data-image');
        let quotes = document.getElementsByClassName("article-body__block__quote");

        let image = document.createElement('img');
        image.src = coverImageUrl;
        image.onload = () => {
            Velocity(cover, {opacity:1}, {duration: 400});
        };

        window.addEventListener("scroll", () => {
            // if(articleHeader.getBoundingClientRect().top > 0 ) {
            //    let top = Math.floor(document.body.scrollTop * 0.4);
            //    coverImage.style.top = top + 'px';
            // }

            if(quotes.length) {
                for (let quote of quotes) {
                    var windowHeight = getWindowHeight();
                    if(quote.getBoundingClientRect().top - windowHeight < -(windowHeight/5) ) {
                        addClass(quote, 'fade-in');
                        quotes = document.querySelectorAll(".article-body__block__quote:not(.fade-in)");
                    }
                }
            }

        });


    }

    static renderAllGalleries() {

        // get all gallery elements
        let galleries = document.getElementsByClassName("article-gallery");

        // iterate through gallery elements
        for (var gallery of galleries) {

            // initialize flickity gallery
            let flkty = new Flickity( gallery, {
                setGallerySize: true,
                contain: true,
                wrapAround: true,
                //lazyLoad: true,
                pageDots: false,
                cellSelector: '.gallery__cell',
                prevNextButtons: false
            });

            ((
                flkty,
                next,
                prev,
                count,
                caption
            ) => {

                // Bind Events to Arrows
                next.addEventListener('click', () => flkty.next());
                prev.addEventListener('click', () => flkty.previous());

                // When displaying next/previous image, ...
                flkty.on( 'cellSelect', () => {

                    // Update Index Reference
                    let currentCell = (flkty.selectedIndex + 1);
                    let totalCells = flkty.cells.length;
                    count.textContent = `${currentCell}/${totalCells}`;

                    // Fade out Caption

                    Velocity(caption, {opacity: 0}, {
                        duration: 200,
                        complete: () => {

                            // Once Caption has faded out, update and fade in again
                            let cpationText = flkty.selectedElement.getAttribute('data-caption');
                            if (caption.textContent !== undefined)
                                caption.textContent = cpationText;
                            else
                                caption.innerText = cpationText;

                            Velocity(caption, {opacity:1}, {duration: 1, delay: 200});

                        }

                    }, 'ease-out');
                });

            })(
                flkty,
                gallery.getElementsByClassName('gallery__next')[0],
                gallery.getElementsByClassName('gallery__prev')[0],
                gallery.getElementsByClassName('gallery__count')[0],
                gallery.getElementsByClassName('gallery__caption')[0]
            );

        }


    }

    static fixArticleImages() {

        const articleImages = document.querySelectorAll('.article-body__block__text img');

        for (var articleImage of articleImages) {
            articleImage.setAttribute('height','');
            articleImage.style.height = '';
        }

    }

}

export { Article }
