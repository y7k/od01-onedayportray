import { addClass, removeClass } from './Helpers';

class Navigation {

    static init() {

        let previousScrollTop = 0;
        const navElements = document.getElementsByClassName("navigation");

        if( navElements.length < 1 ) return;

        const navHeight = navElements[0].offsetHeight;

        window.addEventListener("scroll", () => {

            let scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;

            // hide navigation
            if (scrollTop > previousScrollTop && scrollTop > navHeight) {
                for (let navEl of navElements) {
                    addClass(navEl, 'is-moved-up')
                }

                previousScrollTop = scrollTop;

            // show navigation
            } else if (scrollTop + 66 < previousScrollTop){
                for (let navEl of navElements) {
                    removeClass(navEl, 'is-moved-up')
                }

                previousScrollTop = scrollTop;
            }

            if(scrollTop > 120) {
                for (let navEl of navElements) {
                    addClass(navEl, 'show-logo')
                }
            } else {
                for (let navEl of navElements) {
                    removeClass(navEl, 'show-logo')
                }
            }


        });

    }

}

export { Navigation }