import Masonry from 'masonry-layout';
import imagesLoaded from 'imagesloaded';
import request from 'then-request';
import { getWindowWidth, getDocHeight, addClass } from './Helpers';

class Grid {

    static renderArticleGrids() {

        // get all gallery elements
        const grids = document.getElementsByClassName("article-grid");

        // iterate through gallery elements
        for (let grid of grids) {

            new Masonry( grid, {
                itemSelector: '.article-grid-item',
                columnWidth: 313,
                isFitWidth: true,
                //containerStyle: { maxWidth: '964px' },
                gutter: 12,
                transitionDuration:  '0.1s'
            });

        }
    }

    static renderBlogGrid() {

        // get all gallery elements
        const grid = document.getElementsByClassName("blog-grid")[0];

        const paginateLink = document.getElementById("blog-paginate-next");

        const fullGridWidth = 1150;

        let isLoadingMore = false;

        const oneColumn = {
            itemSelector: '.blog-grid-item',
            columnWidth: 555,
            isFitWidth: true,
            gutter: 40,
            transitionDuration:  '0.1s'
        };

        const twoColumns = {
            itemSelector: '.blog-grid-item',
            columnWidth: 555,
            gutter: 40,
            transitionDuration:  '0.1s',
            stamp: '.blog-grid-stamp'
        };



        let prevWindowWidth = getWindowWidth();

        let options = (prevWindowWidth < fullGridWidth) ? oneColumn : twoColumns;

        let msnry = new Masonry( grid, options);

        let gridItems = document.getElementsByClassName("blog-grid-item");

        for (let gridItem of gridItems) {
            imagesLoaded(gridItem, (e) => {
                Velocity(gridItem, {opacity: 1}, {duration: 200}, 'ease-in-out');
            });
        }

        window.addEventListener('resize', function(event){
            if(
                (prevWindowWidth < fullGridWidth && getWindowWidth() >= fullGridWidth)
                    ||
                (prevWindowWidth >= fullGridWidth && getWindowWidth() < fullGridWidth)
            ) {
                msnry.destroy();
                prevWindowWidth = getWindowWidth();
                options = (prevWindowWidth < fullGridWidth) ? oneColumn : twoColumns;
                msnry = new Masonry( grid, options);
            }
        });

        window.addEventListener("scroll", () => {
            if(document.body.scrollTop + window.innerHeight >= getDocHeight() && ! isLoadingMore) {

                isLoadingMore = true;

                var nextLink = document.getElementById('blog-paginate-next');

                if(! nextLink) return;

                let url = nextLink.getAttribute('href');

                // scrolled to bottom
                request('GET', url).done((res) => {

                    if (res.statusCode >= 300)  {
                        isLoadingMore = false;
                        return;
                    }

                    let body = res.getBody();
                    let el = document.createElement( 'html' );
                    el.innerHTML = body;
                    let blogEntries = el.querySelectorAll('.blog-grid-item');

                    let elems = [];
                    let fragment = document.createDocumentFragment();

                    for (let blogEntry of blogEntries) {

                        blogEntry.style.opacity = '1';
                        addClass(blogEntry, 'has-been-added');

                        fragment.appendChild( blogEntry );
                        elems.push( blogEntry );

                    }

                    // append elements to container
                    grid.appendChild( fragment );
                    // add and lay out newly appended elements
                    msnry.appended( elems );

                    setTimeout(()=>{
                        msnry.layout();
                    },100);

                    let link = el.querySelector('#blog-paginate-next');

                    if(link) {
                        paginateLink.setAttribute('href', link.getAttribute('href'));
                        isLoadingMore = false;
                    }

                });

            }
        });






    }

}

export { Grid }