import Bugsnag from 'bugsnag-js';
import pkg from '../../../../package.json';

class Bugs {

    static nag() {

        Bugsnag.apiKey = "c1e7557091203430e986b54393f120ed";

        Bugsnag.releaseStage = Bugs.detectReleaseStage();

        Bugsnag.appVersion = pkg.version;

        Bugsnag.notifyReleaseStages = ["staging", "production"];

        Bugsnag.beforeNotify = function(payload) {
            // Only notify Bugsnag of errors in `all.js` file
            if( ! payload.file) return true;
            var match = payload.file.match(/all\.js/i);
            return !!(match && match[0].length > 0);
        }

    }

    static detectReleaseStage() {

        let href = window.location.href;

        return ( href.indexOf(".pizza") !== -1 ) ? "staging"
            : (( href.indexOf(".app") !== -1 || href.indexOf(".dev") !== -1 ) ? "development"
            : "production" );
    }

}

export { Bugs }