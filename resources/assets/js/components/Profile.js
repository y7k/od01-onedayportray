import Velocity from 'velocity-animate';
import { addClass, removeClass } from './Helpers';

class Profile {
    
     static initCover() {

        // Fade In Cover Image
        let cover = document.getElementsByClassName("person__cover")[0];
        let coverImage = document.getElementsByClassName("person__cover__image")[0];
        let coverImageUrl = coverImage.getAttribute('data-image');

        let image = document.createElement('img');
        image.src = coverImageUrl;
        image.onload = () => {
            Velocity(cover, {opacity:1}, {duration: 400});
        };


    }

    static initModals() {

        const profileOpeners = document.getElementsByClassName('article-profile-open');
        const articleProfile = document.getElementById('article-profile');

        if(profileOpeners==null || profileOpeners.length<1 || articleProfile==null || articleProfile.length<1) return;

        const profileWrapper = document.getElementById('article-profile-wrapper');
        const profileOverlay = document.getElementsByClassName('article-profile__overlay')[0];
        const profileCloser = document.getElementById('article-profile-modal-close');

        for (let profileOpener of profileOpeners) {

            profileOpener.addEventListener('click', (e) => {

                e.preventDefault();

                // block body scroll
                addClass(document.body, 'disable-scroll');

                // show profile
                removeClass(articleProfile, 'is-hidden');

                Velocity(profileOverlay, {backgroundColorAlpha: 0.8}, {duration: 200}, 'ease-out');
                Velocity(profileWrapper, {translateX: '-50%', translateY: 0}, {duration: 200}, 'ease-in-out');
            });
        }

        var closeProfileModal = () => {

            Velocity(profileCloser, {scale:0}, {duration: 100}, 'ease-out');

            Velocity(profileWrapper, {translateX:'-50%', translateY:'100%'}, {
                duration: 200,
                delay: 200
            }, 'ease-in-out');

            Velocity(profileOverlay, {backgroundColorAlpha:0}, {
                duration: 300,
                delay: 300,
                complete: () => {

                    profileCloser.style.transform = '';

                    removeClass(document.body, 'disable-scroll');
                    addClass(articleProfile, 'is-hidden');

                }
            }, 'ease-in');

        };

        articleProfile.addEventListener('click', (e) => {
            if(e.target==profileWrapper || e.target==profileOverlay) closeProfileModal();
        });

        profileCloser.addEventListener('click', closeProfileModal);



    }

}

export { Profile }
