import Velocity from 'velocity-animate';
import { addClass, removeClass } from './Helpers';

class Search {

    static init() {

        const searchBars = document.getElementsByClassName("searchbar");

        for (let searchBar of searchBars) {
            Search.initSearchBar(searchBar);
        }

    };

    static initSearchBar(el) {

        let searchInput = el.querySelectorAll(".search-input")[0];
        let searchSubmit = el.querySelectorAll(".search-submit")[0];

        let isSearchBarOpen = false;

        let openSearchBar = function(){

            addClass(searchInput, 'is-expanded');
            addClass(searchInput.parentNode.parentNode.parentNode, 'search-is-expanded');

            Velocity(searchInput, {paddingRight:50, paddingLeft:15}, {
                duration: 200,
                complete: () => {
                    searchInput.focus();
                }
            }, 'ease-in-out');

            setTimeout(() => {
                addClass(searchSubmit, 'is-open');
            }, 50);

            isSearchBarOpen = true;
        };

        let closeSearchBar = function(){

            if(searchInput.value.trim().length) return;

            removeClass(searchInput, 'is-expanded');
            removeClass(searchInput.parentNode.parentNode.parentNode, 'search-is-expanded');

            Velocity(searchInput, {paddingRight:0, paddingLeft:0}, {
                duration: 100
            }, 'ease-in-out');

            setTimeout(() => {
                removeClass(searchSubmit, 'is-open');
            }, 75);

            setTimeout(() => {
                isSearchBarOpen = false;
            }, 500);

        };

        searchInput.addEventListener('blur', closeSearchBar);

        searchSubmit.addEventListener('click', (e) => {
            if( ! isSearchBarOpen ) {
                e.preventDefault();
                openSearchBar();
            } else if(!searchInput.value.trim().length) {
                e.preventDefault();
                closeSearchBar();
            }
        });


    }


}

export { Search }