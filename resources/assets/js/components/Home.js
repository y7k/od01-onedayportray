import Swing from 'swing';
import Velocity from 'velocity-animate';
import Color from 'color-js';
import { addClass, removeClass, getWindowWidth, reverseChildNodes, getRandomNumber } from './Helpers';

class Home {

    init() {

        this.introHasPlayed = false;
        this.trowingAllIn = false;

        const cards = document.getElementsByClassName('swing-card');
        this.cards = [].slice.call(cards);

        this.articleMeta = document.getElementById('article-meta');
        this.cardholder = document.getElementById('article-cards');


        let articleColor = this.cardholder.getAttribute('data-home-color');
        this.topColor = this.nextColor = Color(articleColor);

        this.initSwing();
        this.attachDragToCursor();

        setTimeout(() => {
            this.runIntroAnimation();
        }, 1500);

    }

    initSwing() {

        const config = {

            throwOutDistance: () => {
                return getWindowWidth();
            },

            throwOutConfidence: function (offset, element) {
                return Math.min(Math.abs( offset * 2.5 ) / element.offsetWidth, 1);
            },

            rotation: (x, y, element, maxRotation) => {
                let horizontalOffset,
                    rotation,
                    verticalOffset;

                horizontalOffset = Math.min(Math.max(x / element.offsetWidth, -1), 1);
                verticalOffset = (y > 0 ? 1 : -1) * Math.min(Math.abs(y) / 100, 1);
                rotation = horizontalOffset * verticalOffset * maxRotation;

                rotation = rotation + parseInt(element.getAttribute('data-rotation'));

                return rotation;
            }
        };

        this.stack = gajus.Swing.Stack(config);

        this.cards.forEach((card) => {

            // create card
            this.stack.createCard(card);

            // must set rotation for intro to run
            let rotation = getRandomNumber(-3, 3);
            card.setAttribute('data-rotation', rotation);

            // throw card out
            let y = Math.floor(Math.random() * (window.innerHeight * 2 + 1)) - window.innerHeight;
            this.stack.getCard(card).throwOut( [-1,1][Math.round(Math.random())] , y );

        });


        // Add event listener for when a card is thrown out of the stack.
        this.stack.on('throwout', (e) => {

            if(!this.introHasPlayed) return;

            // set classes
            removeClass(e.target, 'is-on-pile');
            addClass(this.articleMeta, 'fade-out');
            addClass(e.target, 'throw-out');

        });

        this.stack.on('throwoutend', (e) => {

            let card = e.target;

            if(!this.introHasPlayed) return;

            // hide card
            setTimeout(() => {
                addClass(card, 'has-been-thrown-out');
                removeClass(card, 'is-on-pile');
                removeClass(card,'is-top-card');
            }, 500);

            if(e.target.getAttribute('data-is-last-article') == "true") {

                setTimeout(() => {
                    this.throwAllBackIn(false);
                }, 500);

            } else {
                this.updateMeta();
                removeClass(this.articleMeta, 'fade-out');
            }

        });

        this.stack.on('throwin', (e) => {

            if(!this.introHasPlayed) return;

            this.articleMeta.style.opacity = '';
            removeClass(this.articleMeta, 'fade-out');

            if( ! this.trowingAllIn ) Velocity(this.cardholder, {'backgroundColor': this.topColor.toString()}, {duration: 200});

        });

        this.stack.on('throwinend', (e) => {

            let card = e.target;

            setTimeout(() => {
                removeClass(card, 'throw-out');
                addClass(card, 'is-on-pile');
                removeClass(card, 'has-been-thrown-out');
            },200);

            this.attachDragToCursor();
        });

        //this.stack.on('dragstart', (e) => {
        //    this.attachDragToCursor();
        //});

        this.stack.on('dragmove', (e) => {
            this.articleMeta.style.opacity = (1 - e.throwOutConfidence * 0.8);

            this.cardholder.style.backgroundColor = this.topColor.blend(this.nextColor,e.throwOutConfidence);
        });

        this.stack.on('dragend', (e) => {
            this.articleMeta.style.opacity = '';
        });


    };

    throwAllBackIn(isIntroAnimation) {

        var _self = this;

        this.trowingAllIn = true;

        if( ! isIntroAnimation) {
            reverseChildNodes(this.cardholder);
            this.cardholder = document.getElementById('article-cards');
        }

        this.cards.forEach((card) => {

            card.setAttribute('data-is-last-article',false);

            let rotation = getRandomNumber(-3, 3);
            card.setAttribute('data-rotation', rotation);

            removeClass(card, 'has-been-thrown-out');
            addClass(card, 'is-on-pile');

            var c = this.stack.getCard(card);
            c.throwIn(0, 0);
        });

        this.cardholder.children[0].setAttribute('data-is-last-article', "true");

        this.updateMeta();

        Velocity(this.cardholder, {'backgroundColor': this.topColor.toString()}, {duration: 500, complete: () => {
            _self.trowingAllIn = false;
        }});
    }

    static getTopCard() {
        let nextCards = document.getElementsByClassName('swing-card is-on-pile');
        // return last card or null if no cards were found
        return (nextCards.length) ? nextCards[nextCards.length-1] : null;
    }

    static getSecondTopCard() {
        let nextCards = document.getElementsByClassName('swing-card is-on-pile');
        // return last card or null if no cards were found
        return (nextCards.length>1) ? nextCards[nextCards.length-2] : null;
    }

    updateMeta() {
        // get top card
        let topCard = Home.getTopCard();
        // stop if no card was found
        if(topCard==null) {
            setTimeout(()=>{
                this.updateMeta();
            },500);
            return;
        }

        let secondTopCard = Home.getSecondTopCard();

        addClass(topCard, 'is-top-card');

        let articleColor = topCard.getAttribute('data-article-color');
        this.topColor = Color(articleColor);

        let nextArticleColor = (secondTopCard==null) ? this.cardholder.getAttribute('data-home-color') : secondTopCard.getAttribute('data-article-color');
        this.nextColor = Color(nextArticleColor);

        // get card info
        let nextMeta = topCard.querySelectorAll('.article-card__meta')[0];
        // update card info
        this.articleMeta.innerHTML = nextMeta.innerHTML;

    }

    runIntroAnimation() {

        this.cards.forEach((card) => {
            addClass(card, 'throw-out');
            addClass(card, 'is-enabled');
        });

        this.throwAllBackIn(true);

        this.cards[0].setAttribute('data-is-last-article',"true");

        setTimeout(() => {
            let logo = document.getElementsByClassName('logo-container')[0];
            let navigation = document.getElementsByClassName('navigation--home')[0];

            addClass(logo, 'is-enabled');
            addClass(navigation, 'is-enabled');

            removeClass(this.articleMeta, 'fade-out');

            this.introHasPlayed = true;
        },500);

    }

    attachDragToCursor() {

        if(Modernizr.touch) return;

        let tooltipWrapper = document.getElementById('article-tooltip-wrapper');
        let tooltipParent = tooltipWrapper.parentNode;

        tooltipParent.removeChild(tooltipWrapper);
        tooltipParent.appendChild(tooltipWrapper);

        let tooltipSpan = document.getElementById('article-tooltip');

        window.onmousemove = function (e) {
            var x = e.clientX,
                y = e.clientY;
            tooltipSpan.style.top = (y - 13) + 'px';
            tooltipSpan.style.left = (x + 16) + 'px';
        };
    }


}

export { Home }