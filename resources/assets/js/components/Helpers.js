import tinycolor from 'tinycolor2';

class Helpers {

    static setSelectColor() {

        // get the "select" color of the aricle
        const article = document.getElementsByClassName('has-article-color')[0];
        if(!article) return;
        const articleColor = article.getAttribute('data-articlecolor');

        // choose black or white as text color (when selected) based on brigthness of the "select" color
        const foregroundColor = (tinycolor(articleColor).isLight()) ? '#000' : '#fff';

        // apply styles to document
        const styles =   `background-color: ${articleColor};
                        color: ${foregroundColor};`;

        try {
            addCSSRule(document.styleSheets[0], "::selection", styles);
        } catch(err) {}

        try {
            addCSSRule(document.styleSheets[0], "::-moz-selection", styles);
        } catch(err) {}

    }
}


function addCSSRule(sheet, selector, rules, index) {
    if("insertRule" in sheet) {
        sheet.insertRule(selector + "{" + rules + "}", index);
    }
    else if("addRule" in sheet) {
        sheet.addRule(selector, rules, index);
    }
}


function addClass(el, className) {
    if (el.classList)
        el.classList.add(className);
    else
        el.className += ' ' + className;
}

function removeClass(el, className) {
    if (el.classList)
        el.classList.remove(className);
    else
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}

function hasClass(el, className) {
    if (el.classList)
        return el.classList.contains(className);
    else
        return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
}


function getWindowWidth() {
    let w = window;
    let d = document;
    let e = d.documentElement;
    let g = d.getElementsByTagName('body')[0];
    let x = w.innerWidth || e.clientWidth || g.clientWidth;
    return x;
}

function getWindowHeight() {
    let w = window;
    let d = document;
    let e = d.documentElement;
    let g = d.getElementsByTagName('body')[0];
    let x = w.innerHeight || e.clientHeight || g.clientHeight;
    return x;
}

function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}

function reverseChildNodes(node) {
    var parentNode = node.parentNode, nextSibling = node.nextSibling,
        frag = node.ownerDocument.createDocumentFragment();
    parentNode.removeChild(node);
    while(node.lastChild)
        frag.appendChild(node.lastChild);
    node.appendChild(frag);
    parentNode.insertBefore(node, nextSibling);
    return node;
}

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function detectOldIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    // other browser
    return false;
}

export {
    Helpers,
    addCSSRule,
    addClass,
    removeClass,
    hasClass,
    getWindowWidth,
    getWindowHeight,
    getDocHeight,
    reverseChildNodes,
    getRandomNumber,
    detectOldIE
}