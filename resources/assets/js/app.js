require("babel/polyfill");
import { Poly } from './components/Polyfills';
import { Bugs } from './components/Bugsnag';
import { Article } from './components/Article';
import { Profile } from './components/Profile';
import { Helpers } from './components/Helpers';
import { Grid } from './components/Grid';
import { Navigation } from './components/Navigation';
import { Search } from './components/Search';
import { Home } from './components/Home';

class App {

    static run() {

        Bugs.nag();

        Poly.fill();

        // Init Navigation
        Navigation.init();
        Search.init();

        switch (
            document.body.getAttribute('data-template')
            ) {

            case 'article':
                Article.init();
                Profile.initModals();
                Grid.renderArticleGrids();
                break;

            case 'archive':
                Grid.renderArticleGrids();
                break;

            case 'blog':
                Grid.renderBlogGrid();
                break;

            case 'person':
                Helpers.setSelectColor();
                Profile.initCover();
                Profile.initModals();
                Grid.renderArticleGrids();
                break;

            case 'home':
                let home = new Home();
                home.init();
                break;
        }

    }

}

App.run();
