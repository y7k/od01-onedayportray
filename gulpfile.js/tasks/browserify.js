var gulp           = require("gulp");
var config         = require('../config');
var browserify     = require('browserify');
var source         = require('vinyl-source-stream');
var livereload     = require('gulp-livereload');
var notify         = require('gulp-notify');
var babelify       = require('babelify');
var uglify         = require('gulp-uglify');

gulp.task('browserify', function() {

    return browserify(config.js.assets.file)
        .transform(babelify, { stage: 0 })
        .bundle()
        .pipe(source(config.js.build.name))
        .pipe(gulp.dest(config.js.build.folder));

});

gulp.task('browserify-watch', function() {

    return browserify(config.js.assets.file)
        .transform(babelify, { stage: 0 })
        .bundle()
        .on('error', notify.onError(function(err)  {
            this.emit('end');
            return err.message;
        }))
        .pipe(source(config.js.build.name))
        .pipe(gulp.dest(config.js.build.folder))
        .pipe( livereload() );

});