var gulp = require('gulp');
var config = require('../config');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');

gulp.task('build', ['compress-vendorScripts', 'compress-css', 'compress-js']);

gulp.task('compress-js', ['browserify'], function() {
    return gulp.src('./public/assets/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./public/assets/js'));
});

gulp.task('compress-css', ['styles'], function() {
    return gulp.src('./public/assets/css/*.css')
        .pipe(cssnano())
        .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('compress-vendorScripts', function () {
    return gulp.src(config.vendor.assets.folder + '/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(config.vendor.build.folder));
});
