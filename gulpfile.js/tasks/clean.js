var gulp = require('gulp');
var config = require('../config');
var del = require('del');

/**
 * Delete build folder
 */
gulp.task('clean', function() {

  del([
      config.js.build.folder,
      config.css.build.folder
    ]);

});
