var gulp = require('gulp');
var config = require('../config');


gulp.task('assets', ['vendorScripts']);

gulp.task('vendorScripts', function () {
  return gulp.src(config.vendor.assets.folder + '/**/*.js')
      .pipe(gulp.dest(config.vendor.build.folder));
});
