var gulp = require('gulp');
var config = require('../config');
var livereload     = require('gulp-livereload');

gulp.task('watch', ['default'], function() {
  livereload.listen();
  gulp.start(['watchStyles', 'watchScripts', 'watchTemplates']);
});

gulp.task('watchStyles', function() {
  gulp.watch(config.css.assets.folder + '/**/*.scss', ['styles']);
});

gulp.task('watchScripts', function() {
  gulp.watch(config.js.assets.folder + '/**/*.js', ['browserify-watch']);
});

gulp.task('watchTemplates', function() {
  gulp.watch('craft/templates/**/*.html', ['reload']);
});

gulp.task('reload', function() {
  gulp.src('')
      .pipe(livereload());
});

