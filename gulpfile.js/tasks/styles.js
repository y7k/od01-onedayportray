var gulp           = require('gulp');
var config         = require('../config');
var sass           = require('gulp-sass');
var autoprefixer   = require('gulp-autoprefixer');
var livereload     = require('gulp-livereload');
var notify         = require('gulp-notify');

var autoPrefixBrowserList = ['> 1%', 'last 2 versions', 'ie 9'];

gulp.task('styles', function() {

    return gulp.src(config.css.assets.file)

        .pipe(sass({
            errLogToConsole: true,
            includePaths: [
                config.css.assets.folder
            ]
        }))

        .on('error', notify.onError(function(err)  {
            return err.message;
        }))

        .pipe(autoprefixer({
            browsers: autoPrefixBrowserList,
            cascade: true
        }))

        .pipe(gulp.dest( config.css.build.folder ))

        .pipe( livereload() );
});