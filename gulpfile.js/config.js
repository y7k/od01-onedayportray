var config = {};

/**
 * Base Paths
 */
config.paths = {
  assets: './resources/assets/',
  build: './public/assets/',
  gulp: './gulpfile.js/'
};

/**
 * App
 */
config.js = {

  assets: {
    folder: config.paths.assets + 'js',
    file: config.paths.assets + 'js/app.js'
  },

  build: {
    folder: config.paths.build + 'js',
    name: 'all.js'
  }

};


/**
 * Vendor Assets config
 */
config.vendor = {
  assets: {
    folder: config.js.assets.folder + '/vendor'
  },
  build: {
    folder: config.js.build.folder + '/vendor'
  }
};



/**
 * CSS Config
 */
config.css = {
  assets: {
    folder: config.paths.assets + 'scss',
    file: config.paths.assets + 'scss/main.scss'
  },

  build: {
    folder: config.paths.build + 'css'
  }

};

/**
 * Fonts Config
 */
config.fonts = {
  assets: {
    folder: config.paths.assets + 'fonts/**'
  },
  build: {
    folder: config.paths.build + 'fonts/'
  }
};

/**
 * Images config
 */
config.images = {
  assets: {
    folder: config.paths.assets + 'images'
  },
  build: {
    folder: config.paths.build + 'images'
  }
};

/**
 * Meta images config
 */
config.meta = {
  assets: {
    folder: config.paths.assets + 'meta'
  },
  build: {
    folder: config.paths.build + 'meta'
  }
};



module.exports = config;

