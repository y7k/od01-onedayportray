<?php

$basePath = __DIR__ . '/../..';

// Load Dotenv Config
require_once $basePath . '/vendor/autoload.php';

$dotenv = new Dotenv\Dotenv($basePath);
$dotenv->load();

// Path to your craft/ folder
$craftPath = $basePath . '/craft/release';
define('CRAFT_BASE_PATH', $basePath . '/craft/');

// Tell Craft to serve the English content
define('CRAFT_LOCALE', 'de');

// Do not edit below this line
$path = rtrim($craftPath, '/').'/app/index.php';

if (!is_file($path))
{
	if (function_exists('http_response_code'))
	{
		http_response_code(503);
	}

	exit('Could not find your craft/ folder. Please ensure that <strong><code>$craftPath</code></strong> is set correctly in '.__FILE__);
}

require_once $path;
