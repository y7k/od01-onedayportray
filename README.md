# One Day Portray

### Specs
_Craft_ CMS as git submodule in `craft/release`

Init by running:
`$ git submodule update --init --recursive`

Update by running:
`$ git submodule foreach git pull origin master`

### Run locally

Install _composer_ dependencies:
```
composer install
```

Make .env file:
```
cp .env.example .env
```

Run _gulp_:
```
npm install
gulp
```

Import Database.

Add Craft License File.


#### Tweak Homestead v0.3.3+ to Work with Craft 2

SSH into your Homestead box using…
`ssh vagrant@127.0.0.1 -p 2222`

Edit the my.cnf file found in /etc/mysql/
`sudo vim /etc/mysql/my.cnf`

Add the following to the bottom of my.cnf :
``
#Set SQL Mode (excluding ONLY_FULL_GROUP_BY)
sql-mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'
```

Save those changes and then restart MySQL to activate them!
`sudo service mysql restart`