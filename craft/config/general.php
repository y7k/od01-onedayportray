<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return [

    '*' => [
        'omitScriptNameInUrls' => true,
        'usePathInfo' => true,
        'generateTransformsBeforePageLoad' => true,
        'convertFilenamesToAscii' => true,
        'enableTemplateCaching' => true,
        'cacheDuration' => 'P2W', // Set default caching time to 2 weeks
    ],

    'od01.app' => [
        'devMode' => true,
        'enableTemplateCaching' => false,

        'siteUrl' => [
            'en' => 'http://od01.app/',
            'de' => 'http://od01.app/de/',
        ],
    ],

    'od01.y7k.pizza' => [
        'devMode' => true,

        'siteUrl' => [
            'en' => 'http://od01.y7k.pizza/',
            'de' => 'http://od01.y7k.pizza/de/',
        ],
    ],

    'onedayportray.com' => [
        'siteUrl' => [
            'en' => 'http://onedayportray.com/',
            'de' => 'http://onedayportray.com/de/',
        ],
    ],
];
