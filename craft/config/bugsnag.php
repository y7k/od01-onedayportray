<?php # craft/config/bugsnag.php

return array(
	'api_key' => getenv('BUGSNAG_API_KEY'),
	'notify_release_stages' => ['production', 'staging']
);
