<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Composer\\Installers\\' => array($vendorDir . '/composer/installers/src'),
    'Bugsnag_' => array($vendorDir . '/bugsnag/bugsnag/src'),
);
