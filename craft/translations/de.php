<?php


return [
    'Did you enjoy the portrait? Share it with your friends:' => 'Hat dir das Portrait gefallen? Teile es mit deinen Freunden:',
    'Related Portraits' => 'Verwandte Portraits',
    'Text by' => 'Text',
    'Photographs by' => 'Fotos',
    'Translation by' => 'Übersetzung',
    'Sponsored By' => 'Sponsored By',
    'Portrayed' => 'Portraitiert im',
    'Read her Profile' => 'Zum Steckbrief',
    'Read his Profile' => 'Zum Steckbrief',
    'Read their Profile' => 'Zum Steckbrief',
    'Want to know more about {Name}?' => 'Willst du mehr über {Name} wissen?',
    'Want to know more?' => 'Willst du mehr darüber wissen?',
    'Read Portrait' => 'Zum Portrait',
    'Search' => 'Suche',
    'Portraits by {Name}' => 'Portraits von {Name}',
    'Results for “{query}“' => 'Suchresultate für “{query}“',
    'Your search for {query} didn\'t return any results.' => 'Deine Suche nach {query} hat leider keine Treffer ergeben. Sollen wir darüber ein Portrait schreiben? Schick uns eine Mail!',
    'Print Portrait' => 'Portrait ausdrucken',
    'Hello friend!%0D%0A I found this portrait on onedayportray.com and thought you\'d like it.%0D%0A%0D%0A{name}: {link}%0D%0A%0D%0AEnjoy!' => 'Hallo Freund/in!%0D%0A%0D%0AIch habe dieses Portrait auf onedayportray.com gefunden und dachte, es könnte dich interessieren.%0D%0A%0D%0A{name}: {link}%0D%0A%0D%0AViel Spass!',
    'Page not found :-(' => 'Seite wurde nicht gefunden :-('
];
